# Changelog 

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.2.0]
### Added
- phive for development tools
- phpunit under phive 
- dephpend under phive
- phpDocumentor under phive
- phploc under phive
- phpcpd under phive
- Web Server
- phpDocumentor configuration example
- .env file support
- composer auto install on image build

### Changed
- reset incomplete changelog
- using php 7.2
- updates symfony dependencies
- phpunit over phive instead of composer
- phploc over phive instead of composer
- phpcpd over phive instead of composer
- using docker as isolated environment
- update phpunit.xml using autoloader instead of bootstrap file
- exmplae checking php 7.2

### REMOVED
- phpmetrics
- phpdoc

