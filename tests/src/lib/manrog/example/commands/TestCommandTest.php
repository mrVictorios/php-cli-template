<?php

namespace manrog\example\commands;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class TestCommandTest
 * @package manrog\example\commands
 */
class TestCommandTest extends TestCase
{
    /** @var TestCommand */
    private $command;
    /** @var InputInterface|\PHPUnit_Framework_MockObject_MockObject */
    private static $inputMock;
    /** @var OutputInterface|\PHPUnit_Framework_MockObject_MockObject */
    private static $outputMock;

    /**
     * @throws \ReflectionException
     */
    protected function setUp()
    {
        parent::setup();

        $this->command = new TestCommand();

        self::$inputMock = $this->getMockForAbstractClass(InputInterface::class);
        self::$outputMock = $this->getMockForAbstractClass(OutputInterface::class);
    }

    public function testExecute_shouldPrintText()
    {
        self::$outputMock->expects($this->once())
            ->method('writeln')
            ->with($this->equalTo('<info>application is ready</info>'));

        $this->command->execute(self::$inputMock, self::$outputMock);
    }
}
