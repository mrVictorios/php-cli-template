<?php

namespace manrog;

use PHPUnit\Framework\TestCase;
use Faker\Factory;

/**
 * Class Faker_Supported_TestCase
 */
class Faker_Supported_TestCase extends TestCase
{
    /** @var Factory */
    protected $faker;

    protected function setUp()
    {
        parent::setUp();

        $this->faker = Factory::create();
    }
}
