# CLI Template

A small project template. With pre configured Code Quality Tools. For CLI Applications Based on Symfony Components.

## Includes
* php7.2
* apache
* mysql

## Usage
### After Setup
```bash
 $ docker-compose start
 $ docker-compose exec php bash
```
### Setup
1. get the template
    ```bash
    $ composer create-project manrog/php-cli-template %PROJECT_NAME%
    ```
2. setup the template (in Example)
    ```bash
    $ cp example.env .env
    ```
3. Build
    ```bash
    $ docker-compose up -d
    ```
4. Get in and have fun
    ```bash
    $ docker-compose exec php bash
    ```
    
5. (Optional)
    ```bash
    <php> $ phive install
    ```
    
    this step is sepratly required on phive. more about this (https://github.com/phar-io/phive/issues/66)

Composer dependencies will automaticly installed for you.  

## Template Overview

### env
Contains Docker Image build informations, if you need more php extension you should edit here.
Base Image is Debian Jessie.
#### apache
Default configuration. Mod Rewrite enabled by default.
Document Root changeable over .env file. (APACHE_DOCUMENT_ROOT)
#### php
Installed Packages on default:
* php7.2-cli
* php7.2-curl
* php7.2-dev
* php7.2-dom
* php7.2-mbstring
* php7.2-json
* php7.2-mysqli
* php7.2-xml
* php-xdebug
* composer
* phive

### src
Contains your source code. The Template works with Symfony Components. With an example

### tests
Contains your test code.

### tools
* composer
* phive
    * phpunit
    * phpcpd
    * phploc
    * phpdocumentor
    * dephpend
