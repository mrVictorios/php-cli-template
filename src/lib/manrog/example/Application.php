<?php

namespace manrog\example;

use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class Application
 *
 * @package manrog\example
 */
class Application extends ConsoleApplication
{
    use ContainerAwareTrait;
}
