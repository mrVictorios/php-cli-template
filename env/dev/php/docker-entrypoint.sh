#!/usr/bin/env bash

if [ "COMPOSER_RUN_INSTALL" ]; then
    cd /var/www
    composer install
fi

echo 'check for syntax errors'
find . -name "*.php" -exec php -l {} \; 1>/dev/null

mkdir build

echo '---------------------------'
echo ''
echo '      READY FOR USAGE'
echo ''
echo '---------------------------'

apache2ctl -DFOREGROUND
